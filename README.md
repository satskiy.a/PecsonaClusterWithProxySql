==DOCKER==

docker run -d \
  -e MYSQL_ROOT_PASSWORD=root \
  -e CLUSTER_NAME=cluster1 \
  -e DISCOVERY_SERVICE=80.211.158.181:2379 \
  --name=node1 \
  --net=pxc-network \
  percona/percona-xtradb-cluster:5.7


docker run -d \
  -e MYSQL_ROOT_PASSWORD=root \
  -e CLUSTER_NAME=cluster1 \
  -e DISCOVERY_SERVICE=80.211.158.181:2379 \
  --name=node2 \
  --net=pxc-network \
  percona/percona-xtradb-cluster:5.7



docker run -d \
  -e MYSQL_ROOT_PASSWORD=root \
  -e CLUSTER_NAME=cluster1 \
  -e DISCOVERY_SERVICE=80.211.158.181:2379 \
  --name=node3 \
  --net=pxc-network \
  percona/percona-xtradb-cluster:5.7




docker run -d -p 3306:3306 -p 6032:6032 --net=pxc-network --name=cluster1_proxysql \
        -e CLUSTER_NAME=cluster1 \
        -e DISCOVERY_SERVICE=80.211.158.181:2379 \
        -e MYSQL_ROOT_PASSWORD=root \
        -e MYSQL_PROXY_USER=proxyuser \
        -e MYSQL_PROXY_PASSWORD=s3cret \
        perconalab/proxysql
        
        
  + https://coreos.com/etcd/docs/latest/v2/docker_guide.html   FOR    DISCOVERY_SERVICE
  
  INF USED
  https://www.percona.com/blog/2016/06/14/scaling-percona-xtradb-cluster-proxysql-docker-swarm/
  https://www.percona.com/doc/percona-xtradb-cluster/LATEST/install/docker.html
  https://www.percona.com/blog/2016/08/03/testing-docker-multi-host-network-performance/
  
  
  + proxysql can be  upgraded
  
  
        